package com.masroor.hw2;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Masroor on 03/03/2018.
 */

public class MyAdapter extends RecyclerView.Adapter{
    ArrayList<Subject> subjects;
    int customLayoutID;
    public MyAdapter(ArrayList<Subject> subjects,int customLayoutID) {
        this.subjects = subjects;
        this.customLayoutID=customLayoutID;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(customLayoutID,parent,false);
        v.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String subject=((TextView)v.findViewById(R.id.subjectTitleView)).getText().toString();
                Subject ref=MainActivity.getSubjectByTitle(subject);
                if(ref.isWeightages_assigned()){
                    Intent i=new Intent(v.getContext(),ProgressSummary.class);
                    //pass the subject name
                    i.putExtra(MainActivity.SUBJECT,subject);
                    v.getContext().startActivity(i);
                }else{
                    Toast.makeText(v.getContext(),"Assign weightages for this subject.",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh=(ViewHolder)holder;
        Subject s=subjects.get(position);
        vh.setParameters(s.getTitle(),s.getObtained_total(),s.getTotal(),s.getObtained_total());
    }
    @Override
    public int getItemCount() {
        return subjects.size();
    }
}
