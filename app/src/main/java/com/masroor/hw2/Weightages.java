package com.masroor.hw2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

//this class takes input of weightages from user and returns them along with the subject title to the MainActivity
//the MainActivity then populates the corresponding subject object's data
public class Weightages extends AppCompatActivity {
    int f,p,m1,m2,a;
    String currentSubject;
    EditText finalW,projW,mid1W,mid2W,assignsW;
    TextView errorText;
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weightages);

        spinner=findViewById(R.id.spinner);
        ArrayList subjects_in_spinner=new ArrayList<>();
        ArrayAdapter arrAdpt=new ArrayAdapter(this,android.R.layout.simple_spinner_item,subjects_in_spinner);
        for (int i=0;i<MainActivity.subjects.size();i++){
            subjects_in_spinner.add(MainActivity.subjects.get(i).getTitle());
        }
        spinner.setAdapter(arrAdpt);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentSubject=spinner.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        finalW=findViewById(R.id.finalWeightage);
        projW=findViewById(R.id.projectWeightage);
        mid1W=findViewById(R.id.mid1Weightage);
        mid2W=findViewById(R.id.mid2Weightage);
        assignsW=findViewById(R.id.assignmentWeightage);
        errorText=findViewById(R.id.errorText);
    }

    public void processIt(View v){
        String f_str=finalW.getText().toString();
        String p_str=projW.getText().toString();
        String m1_str=mid1W.getText().toString();
        String m2_str=mid2W.getText().toString();
        String a_str=assignsW.getText().toString();

        if(!f_str.isEmpty() && !p_str.isEmpty() && !m1_str.isEmpty() && !m2_str.isEmpty() && !a_str.isEmpty()){
            f=Integer.parseInt(f_str);
            p=Integer.parseInt(p_str);
            m1=Integer.parseInt(m1_str);
            m2=Integer.parseInt(m2_str);
            a=Integer.parseInt(a_str);
        }else{
            Toast.makeText(this,"Please fill all details first.",Toast.LENGTH_SHORT).show();
        }

        if((f+p+m1+m2+a)==100){
            Log.i("okay","Sum 100");
            Intent returnIntent=new Intent();
            returnIntent.putExtra(MainActivity.FINAL_WEIGHTAGE,f);
            returnIntent.putExtra(MainActivity.PROJECT_WEIGHTAGE,p);
            returnIntent.putExtra(MainActivity.MID1_WEIGHTAGE,m1);
            returnIntent.putExtra(MainActivity.MID2_WEIGHTAGE,m2);
            returnIntent.putExtra(MainActivity.ASSIGNMENTS_WEIGHTAGE,a);
            returnIntent.putExtra(MainActivity.SUBJECT,currentSubject);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }else{
            errorText.setText(R.string.error);
            errorText.setVisibility(View.VISIBLE);
        }
    }
}
