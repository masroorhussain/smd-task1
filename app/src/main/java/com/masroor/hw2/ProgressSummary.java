package com.masroor.hw2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ProgressSummary extends AppCompatActivity {

    //TextView finalMarks,projectMarks,mid1Marks,mid2Marks,assignMarks;
    /////////////////////////////////////////////////
    EditText a1Marks,a1Total,a2Marks,a2Total,a3Marks,a3Total,projMarks,projTotal,m1Marks,m1Total,m2Marks,m2Total,finalMarks,finalTotal;
    TextView assignmentMarksV,projectMarksV,mid1MarksV,mid2MarksV,finalMarksV;
    String subject_to_process;
    Subject subjectRef;
    int a1Marks_,a1Total_,a2Marks_,a2Total_,a3Marks_,a3Total_,projMarks_,projTotal_,m1Marks_,m1Total_,m2Marks_,m2Total_,finalMarks_,finalTotal_;

    boolean calculations_done=false;
    int w_f,w_p,w_m1,w_m2,w_a;  //weightages
    float m_f,m_p,m_m1,m_m2,m_a;  //absolute marks
    /////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_summary);
        Intent rcvdIntent = getIntent();
        Bundle dataBundle = rcvdIntent.getExtras();
        subject_to_process = (String) dataBundle.get(MainActivity.SUBJECT);
        subjectRef = MainActivity.getSubjectByTitle(subject_to_process);

        w_p = subjectRef.getProject_weightage();
        w_f = subjectRef.getFinal_weightage();
        w_m1 = subjectRef.getMid1_weightage();
        w_m2 = subjectRef.getMid2_weightage();
        w_a = subjectRef.getAssignments_weightage();

        a1Marks = findViewById(R.id.a1Marks);a2Marks = findViewById(R.id.a2Marks);a3Marks = findViewById(R.id.a3Marks);
        a1Total = findViewById(R.id.a1Total);a2Total = findViewById(R.id.a2Total);a3Total = findViewById(R.id.a3Total);
        projMarks = findViewById(R.id.projectMarks);projTotal = findViewById(R.id.projTotal);m1Marks = findViewById(R.id.mid1Marks);
        m2Marks = findViewById(R.id.mid2Marks);m1Total = findViewById(R.id.m1Total);m2Total = findViewById(R.id.m2Total);
        finalMarks = findViewById(R.id.finalMarks);finalTotal = findViewById(R.id.finalTotal);
        assignmentMarksV=findViewById(R.id.assign);projectMarksV=findViewById(R.id.proj);
        mid1MarksV=findViewById(R.id.m1);mid2MarksV=findViewById(R.id.m2);finalMarksV=findViewById(R.id.f);
    }

    public void processIt(View v){
        if(getMarks()){
            //place the calculated absolute marks in the returning intent
            MainActivity.getSubjectByTitle(subject_to_process).setFinal_marks(m_f);
            MainActivity.getSubjectByTitle(subject_to_process).setProject_marks(m_p);
            MainActivity.getSubjectByTitle(subject_to_process).setMid1_marks(m_m1);
            MainActivity.getSubjectByTitle(subject_to_process).setMid2_marks(m_m2);
            MainActivity.getSubjectByTitle(subject_to_process).setAssignments_marks(m_a);
            float obtained_total=m_f+m_p+m_m1+m_m2+m_a;
            float total=MainActivity.getSubjectByTitle(subject_to_process).getTotalMarks();
            MainActivity.getSubjectByTitle(subject_to_process).setObtained_total((int)obtained_total);
            MainActivity.getSubjectByTitle(subject_to_process).setTotal((int)total);
            calculations_done=true;
        }
        if(calculations_done){
            finalMarksV.setText(String.valueOf(MainActivity.getSubjectByTitle(subject_to_process).getFinal_marks()));
            projectMarksV.setText(String.valueOf(MainActivity.getSubjectByTitle(subject_to_process).getProject_marks()));
            mid1MarksV.setText(String.valueOf(MainActivity.getSubjectByTitle(subject_to_process).getMid1_marks()));
            mid2MarksV.setText(String.valueOf(MainActivity.getSubjectByTitle(subject_to_process).getMid2_marks()));
            assignmentMarksV.setText(String.valueOf(MainActivity.getSubjectByTitle(subject_to_process).getAssignments_marks()));
            MainActivity.myAdapter.notifyDataSetChanged();
        }
    }

    public boolean getMarks(){

        boolean done=false;

        String a1_marks_str=a1Marks.getText().toString(),a2_marks_str=a2Marks.getText().toString(),a3_marks_str=a3Marks.getText().toString();
        String a1_total_str=a1Total.getText().toString(),a2_total_str=a2Total.getText().toString(),a3_total_str=a3Total.getText().toString();

        String m1_marks_str=a1Marks.getText().toString(),m2_marks_str=m2Marks.getText().toString();
        String m1_total_str=m1Total.getText().toString(),m2_total_str=m2Total.getText().toString();

        String f_marks_str=finalMarks.getText().toString();
        String f_total_str=finalTotal.getText().toString();

        String p_marks_str=projMarks.getText().toString();
        String p_total_str=projTotal.getText().toString();

        if(
                !a1_marks_str.isEmpty() && !a2_marks_str.isEmpty() && !a3_marks_str.isEmpty()
                        &&
                        !a1_total_str.isEmpty() && !a2_total_str.isEmpty() && !a3_total_str.isEmpty()
                        &&
                        !m1_marks_str.isEmpty() && !m2_marks_str.isEmpty()
                        &&
                        !m1_total_str.isEmpty() && !m2_total_str.isEmpty()
                        &&
                        !f_marks_str.isEmpty()  && !f_total_str.isEmpty()
                        &&
                        !p_marks_str.isEmpty()  && !p_total_str.isEmpty()
                )
        {
            a1Marks_=Integer.parseInt(a1_marks_str);    a1Total_=Integer.parseInt(a1_total_str);
            a2Marks_=Integer.parseInt(a2_marks_str);    a2Total_=Integer.parseInt(a2_total_str);
            a3Marks_=Integer.parseInt(a3_marks_str);        a3Total_=Integer.parseInt(a3_total_str);
            projMarks_=Integer.parseInt(p_marks_str);    projTotal_=Integer.parseInt(p_total_str);
            m1Marks_=Integer.parseInt(m1_marks_str);    m1Total_=Integer.parseInt(m1_total_str);
            m2Marks_=Integer.parseInt(m2_marks_str);    m2Total_=Integer.parseInt(m2_total_str);
            finalMarks_=Integer.parseInt(f_marks_str);  finalTotal_=Integer.parseInt(f_total_str);

            //calculate the absolutes
            m_f=(float)(finalMarks_*w_f)/finalTotal_;
            m_p=(float)(projMarks_*w_p)/projTotal_;
            m_m1=(float)(m1Marks_*w_m1)/m1Total_;
            m_m2=(float)(m2Marks_*w_m2)/m2Total_;
            m_a=(float)((a1Marks_+a2Marks_+a3Marks_)/(a1Total_+a2Total_+a3Total_)*w_a);

            Log.i("absolutes",""+m_f+","+m_p+","+m_m1+","+m_m2+","+m_a);

            done=true;
            return done;

        }else{
            Toast.makeText(this,"Please fill all details first.", Toast.LENGTH_SHORT).show();
        }
        return done;
    }
}

