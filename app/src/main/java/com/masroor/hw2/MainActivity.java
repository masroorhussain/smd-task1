package com.masroor.hw2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //final,proj,mid1,2,assign              the sequence
    static ArrayList<Subject> subjects=new ArrayList<>();
    RecyclerView rv;    static MyAdapter myAdapter;
    static final String SUBJECT="subject";
    private String selected_subject;
    static final String FINAL_WEIGHTAGE="final_weightage";
    static final String PROJECT_WEIGHTAGE="project_weightage";
    static final String MID1_WEIGHTAGE="mid1_weightage";
    static final String MID2_WEIGHTAGE="mid2_weightage";
    static final String ASSIGNMENTS_WEIGHTAGE="assignments_weightage";

    static final String FINAL_MARKS="final_marks";
    static final String PROJECT_MARKS="project_marks";
    static final String MID1_MARKS="mid1_marks";
    static final String MID2_MARKS="mid2_marks";
    static final String ASSIGNMENTS_MARKS="assignments_marks";

    private int final_weightage; float final_marks;
    private int project_weightage; float project_marks;
    private int mid1_weightage; float mid1_marks;
    private int mid2_weightage; float mid2_marks;
    private int assignments_weightage; float assignments_marks;

    static final int REQ_CODE_Weightages=234;
    static final int REQ_CODE_Assessment_Summary=235;
    static final int REQ_CODE_Progress_Summary=236;


    static{
        subjects.add(new Subject("Software for Mobile Devices"));
        subjects.add(new Subject("Cloud Computing"));
        subjects.add(new Subject("OOAD"));
        subjects.add(new Subject("Software Engineering"));
        subjects.add(new Subject("Data Science"));
    }
    {
        for (int i = 0; i < subjects.size(); i++) {
            subjects.get(i).setFinal_weightage(final_weightage);    subjects.get(i).setProject_weightage(project_weightage);
            subjects.get(i).setMid1_weightage(mid1_weightage);  subjects.get(i).setMid2_weightage(mid2_weightage);
            subjects.get(i).setAssignments_weightage(assignments_weightage);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv=findViewById(R.id.recyclerView);
        myAdapter=new MyAdapter(subjects,R.layout.row_item);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(myAdapter);

        Log.i("info","Adapter to recyclerview is set");
    }

    public void method(View v){
        int id=v.getId();
        switch (id){
            case R.id.manageWeightagesBtn:{
                Intent i=new Intent(this,Weightages.class);
                startActivityForResult(i,REQ_CODE_Weightages);
            }
            break;
        }
    }

    public static Subject getSubjectByTitle(String title){
        for (int i = 0; i < subjects.size(); i++) {
            Subject ref=subjects.get(i);
            if(title.equals(ref.getTitle())){
                return ref;
            }
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            switch (requestCode){
                case REQ_CODE_Weightages:{

                    final_weightage=(int)data.getExtras().get(FINAL_WEIGHTAGE);
                    project_weightage=(int)data.getExtras().get(PROJECT_WEIGHTAGE);
                    mid1_weightage=(int)data.getExtras().get(MID1_WEIGHTAGE);
                    mid2_weightage=(int)data.getExtras().get(MID2_WEIGHTAGE);
                    assignments_weightage=(int)data.getExtras().get(ASSIGNMENTS_WEIGHTAGE);
                    selected_subject=(String)data.getExtras().get(SUBJECT);

                    for (int i = 0; i < subjects.size(); i++) {
                        Subject ref=subjects.get(i);
                        if(selected_subject.equals(ref.getTitle())){
                            ref.setFinal_weightage(final_weightage);    ref.setProject_weightage(project_weightage);
                            ref.setMid1_weightage(mid1_weightage);  ref.setMid2_weightage(mid2_weightage);
                            ref.setAssignments_weightage(assignments_weightage);
                            ref.setWeightages_assigned(true);
                        }
                    }
                }
                break;
//                case REQ_CODE_Assessment_Summary:{
//
//                    final_marks=(float)data.getExtras().get(FINAL_MARKS);
//                    project_marks=(float)data.getExtras().get(PROJECT_MARKS);
//                    mid1_marks=(float)data.getExtras().get(MID1_MARKS);
//                    mid2_marks=(float)data.getExtras().get(MID2_MARKS);
//                    assignments_marks=(float)data.getExtras().get(ASSIGNMENTS_MARKS);
//                }
//                break;
//                case REQ_CODE_Progress_Summary:{
//
//                }
//                break;
            }
        }
    }
}
