package com.masroor.hw2;

/**
 * Created by Masroor on 03/03/2018.
 */

public class Subject {
    private String title;
    private int final_weightage; float final_marks;
    private int project_weightage; float project_marks;
    private int mid1_weightage; float mid1_marks;
    private int mid2_weightage; float mid2_marks;
    private int assignments_weightage; float assignments_marks;
    private boolean weightages_assigned;

    public boolean isWeightages_assigned() {
        return weightages_assigned;
    }

    public void setWeightages_assigned(boolean weightages_assigned) {
        this.weightages_assigned = weightages_assigned;
    }

    private int obtained_total,total;

    public Subject(String title){
        this.title=title;
    }

    public int getObtained_total() {
        return obtained_total;
    }

    public void setObtained_total(int obtained_total) {
        this.obtained_total = obtained_total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFinal_weightage() {
        return final_weightage;
    }

    public void setFinal_weightage(int final_weightage) {
        this.final_weightage = final_weightage;
    }

    public float getFinal_marks() {
        return final_marks;
    }

    public void setFinal_marks(float final_marks) {
        this.final_marks = final_marks;
    }

    public int getProject_weightage() {
        return project_weightage;
    }

    public void setProject_weightage(int project_weightage) {
        this.project_weightage = project_weightage;
    }

    public float getProject_marks() {
        return project_marks;
    }

    public void setProject_marks(float project_marks) {
        this.project_marks = project_marks;
    }

    public int getMid1_weightage() {
        return mid1_weightage;
    }

    public void setMid1_weightage(int mid1_weightage) {
        this.mid1_weightage = mid1_weightage;
    }

    public float getMid1_marks() {
        return mid1_marks;
    }

    public void setMid1_marks(float mid1_marks) {
        this.mid1_marks = mid1_marks;
    }

    public int getMid2_weightage() {
        return mid2_weightage;
    }

    public void setMid2_weightage(int mid2_weightage) {
        this.mid2_weightage = mid2_weightage;
    }

    public float getMid2_marks() {
        return mid2_marks;
    }

    public void setMid2_marks(float mid2_marks) {
        this.mid2_marks = mid2_marks;
    }

    public int getAssignments_weightage() {
        return assignments_weightage;
    }

    public void setAssignments_weightage(int assignments_weightage) {
        this.assignments_weightage = assignments_weightage;
    }

    int getTotalMarks(){
        return assignments_weightage+project_weightage+mid1_weightage+mid2_weightage+final_weightage;
    }

    public float getAssignments_marks() {
        return assignments_marks;
    }

    public void setAssignments_marks(float assignments_marks) {
        this.assignments_marks = assignments_marks;
    }
}
