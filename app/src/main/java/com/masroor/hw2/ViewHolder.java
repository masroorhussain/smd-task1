package com.masroor.hw2;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Masroor on 03/03/2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    TextView subjectTitleView,progressTextView;
    ProgressBar progressBarView;

    public ViewHolder(View itemView) {
        super(itemView);
        this.subjectTitleView=itemView.findViewById(R.id.subjectTitleView);
        this.progressTextView=itemView.findViewById(R.id.progressMessage);
        this.progressBarView=itemView.findViewById(R.id.progressBar);
    }
    public void setParameters(String s,float x,float y,int progress){
        subjectTitleView.setText(s);
        String msg="Progress "+x+"/"+y;
        progressTextView.setText(msg);
        progressBarView.setProgress(progress);
    }
}